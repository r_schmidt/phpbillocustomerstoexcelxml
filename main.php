<?php
if (array_key_exists("REQUEST_METHOD", $_SERVER)) {
  die("Test script is to be called via CLI only.");
}

require "lib/BillomatDomDocument.class.php";
require "lib/BillomatPropDomDocument.class.php";
require "lib/BillomatCustomerDomDocument.class.php";
require "lib/BillomatRequest.class.php";
require "lib/MyBillomatRequest.class.php";

try {
  $bmReq = new MyBillomatRequest();

  /**
   * @var BillomatCustomerDomDocument $xmlClientDoc
   */
  $xmlClientDoc = $bmReq->doRequest("/api/clients", new BillomatCustomerDomDocument());
  $xmlClientDoc->fetchCustomers($bmReq);

  $transformedDoc = $xmlClientDoc->transformToExcel2003(__DIR__ . '/xml/generic_xml_to_excel2003_xml.xsl');

  file_put_contents($bmReq->targetXmlFile, $transformedDoc->saveXML());
  printf("Transformed XML saved to %s\n", $bmReq->targetXmlFile);
} catch (Exception $e) {
  printf("%s\n", $e->getMessage());
  exit(1);
}