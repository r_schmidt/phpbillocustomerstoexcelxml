<?php
/**
 * Custom Billomat request
 *
 * @package Billomat
 * @author  Rene Schmidt <rene@reneschmidt.de>
 * @licence GPL-v3 or proprietary commercial (on request)
 */
class MyBillomatRequest extends BillomatRequest
{
  /**
   * Your Billomat ID
   *
   * @var string
   */
  protected $billomatId = "your_billomat_id";

  /**
   * Your Billomat API key
   *
   * Get a Billomat API key:
   *
   * 1. Log into your Billomat account.
   * 2. In the page header, click your user name.
   * 3. Click "API-Schlüssel anzeigen", note it along with your Billomat ID.
   *
   * @var string
   */
  protected $apiKey = "your_billomat_api_key";
}
