<?php

/**
 * Billomat DomDocument class
 *
 * @package Billomat
 * @author  Rene Schmidt <rene@reneschmidt.de>
 * @licence GPL-v3 or proprietary commercial (on request)
 */
class BillomatDomDocument extends DOMDocument
{

  /**
   * Constructor
   *
   * @param string $version  Version
   * @param string $encoding Encoding
   */
  public function __construct($version = "1.0", $encoding = "utf-8")
  {
    $this->preserveWhiteSpace = false;
    $this->formatOutput = true;
  }

  /**
   * Create node attribute.
   *
   * This is mostly to avoid type hinting errors because setAttribute() is a member of DOMElement while appendChild
   * returns a DOMNode object. This must cause a hinting error. OTOH, you CAN call setAttribute on a DOMNode as
   * shown here: http://php.net/manual/en/domelement.setattribute.php
   *
   * This little method avoids hinting errors AND is legal to use.
   *
   * @param string $name Name of attribute
   * @param mixed  $val  Value of attribute
   *
   * @return DOMAttr
   */
  public function makeAttr($name, $val)
  {
    $xmlAttr = $this->createAttribute($name);
    $xmlAttr->nodeValue = $val;

    return $xmlAttr;
  }

  /**
   * Replace all chars that would make an invalid xml element name
   *
   * @param string $str String to manipulate
   *
   * @return mixed
   */
  public function getValidNodeName($str)
  {
    return preg_replace("/--/", "-", preg_replace("/[äöüß_  ]/i", "-", strtolower($str)));
  }

  /**
   * Check if $val is empty, if so, return $default value. if not, return $val
   *
   * @param mixed $val     Value
   * @param mixed $default Default value
   *
   * @return mixed
   */
  public function getNodeValueOrDefault($val, $default)
  {
    if (empty($val)) {
      return $default;
    }
    return html_entity_decode($val);
  }
}
