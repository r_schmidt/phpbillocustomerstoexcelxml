<?php
/**
 * Billomat request
 *
 * @package Billomat
 * @author  Rene Schmidt <rene@reneschmidt.de>
 * @licence GPL-v3 or proprietary commercial (on request)
 */
abstract class BillomatRequest
{
  /**
   * @var string
   */
  public $tempXmlFile = "";

  /**
   * @var string
   */
  public $targetXmlFile = "";

  /**
   * @var string
   */
  public $acceptContentType = "application/xml";

  /**
   * @var string
   */
  protected $apiKey = "";

  /**
   * @var string
   */
  protected $billomatId = "";

  /**
   * @var int
   */
  public $returnCode = -1;

  /**
   * Constructor
   */
  public function __construct()
  {
    $this->tempXmlFile = "/tmp/" . uniqId("billomat_temp_", true);
    $this->targetXmlFile = "./open_in_excel_or_openoffice_" . hash("md5", uniqId("", true)) . ".xml";
  }

  /**
   * Check request requirements
   *
   * @return bool
   * @throws Exception
   */
  public function checkRequirements()
  {
    if (empty($this->tempXmlFile)) {
      throw new Exception("No target file given");
    }

    if (empty($this->acceptContentType)) {
      throw new Exception("No content type given");
    }

    if (empty($this->apiKey)) {
      throw new Exception("No api key given");
    }

    return true;
  }

  /**
   * Scan request result for errors
   *
   * @param DOMDocument $doc Returned result
   *
   * @throws Exception
   * @return void
   */
  protected function throwOnError(DOMDocument $doc)
  {
    $xpathProp = new DOMXpath($doc);
    $propNameNodes = $xpathProp->query('/errors/error');

    $errors = array();

    foreach ($propNameNodes AS $propNameNode) {
      $errors[] = $propNameNode->nodeValue;
    }

    if (!empty($errors)) {
      $this->returnCode = -1;
      throw new Exception("Errors found: " . implode(", ", $errors));
    }
  }

  /**
   * Do request
   *
   * @param string      $url URL to request, e.g. /api/client-property-values?client_id={client_id}
   * @param DOMDocument $doc Document to populate
   *
   * @return DOMDocument
   * @throws Exception
   */
  public function doRequest($url, DOMDocument $doc)
  {
    $this->checkRequirements();

    $cmd = "curl -s -H 'X-BillomatApiKey: %s' -H 'Accept: %s' https://%s.billomat.net%s > %s";

    exec(
      sprintf($cmd, $this->apiKey, $this->acceptContentType, $this->billomatId, $url, $this->tempXmlFile),
      $output,
      $this->returnCode
    );

    if ((int)$this->returnCode <> 0) {
      throw new Exception(sprintf("Error during request: %s\n", $output));
    }

    $doc->load($this->tempXmlFile);

    $this->throwOnError($doc);

    return $doc;
  }
}
