<?php
/**
 * Billomat customer DOMDocument
 *
 * @package Billomat
 * @author  Rene Schmidt <rene@reneschmidt.de>
 * @licence GPL-v3 or proprietary commercial (on request)
 */
class BillomatCustomerDomDocument extends BillomatDomDocument
{
  /**
   * Fetch customers
   *
   * @param BillomatRequest $bmReq
   */
  public function fetchCustomers(BillomatRequest $bmReq)
  {
    /**
     * @var BillomatDomDocument $xmlPropDoc
     */
    $xpathClient = new DOMXpath($this);
    $clientIdNodes = $xpathClient->query('/clients/client/id');

    foreach ($clientIdNodes AS $clientIdNode) {
      $id = (int)$clientIdNode->nodeValue;
      $xmlPropDoc = $bmReq->doRequest("/api/client-property-values?client_id=" . $id, new BillomatPropDomDocument());

      $xpathProp = new DOMXpath($xmlPropDoc);
      $propNameNodes = $xpathProp->query('/client-property-values/client-property-value/name');

      foreach ($propNameNodes AS $propNameNode) {
        $newNodeName = $xmlPropDoc->getValidNodeName($propNameNode->nodeValue);
        $newNodeValue = $xmlPropDoc->getNodeValueOrDefault($propNameNode->nextSibling->nodeValue, "-");

        $newNode = $clientIdNode->parentNode->appendChild($this->createElement($newNodeName));
        $newNode->nodeValue = $newNodeValue;
      }

      $clientIdNode->parentNode->appendChild($this->makeAttr('id', $id));
    }
  }

  /**
   * Transform document to Excel 2003 XML document
   *
   * @param string $xslFileName XSL file name
   *
   * @return DOMDocument
   */
  public function transformToExcel2003($xslFileName)
  {
    $xsl = new DOMDocument;
    $xsl->load($xslFileName);

    $xsltProc = new XSLTProcessor;
    $xsltProc->importStyleSheet($xsl);

    return $xsltProc->transformToDoc($this);
  }
}
