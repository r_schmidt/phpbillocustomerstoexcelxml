# PhpBilloCustomersToExcelXml
Immature. Do not use.

This is a no-frills exporter that fetches Billomat customers via API and produces XML that can be read
by Excel, OpenOffice, and LibreOffice. It's written in PHP.

This project is meant to be implemented into some larger software. It is not a standalone program.

# Author

Me:

1. https://reneschmidt.de/wiki/index.php/page/view/PhpBilloCustomersToExcelXml,Start
2. https://reneschmidt.de/
3. [I am available for hire](mailto:wiki@reneschmidt.de)

# Licence

GPL v3 or commercial licence :) from github@reneschmidt.de. Do not use this in your closed source project
without paying me. I don't like that.

# Source/Download

[Source can be found at BitBucket](https://bitbucket.org/r_schmidt/phpbillocustomerstoexcelxml/src)

# Requirements

1. PHP (tested with PHP 5.4)
2. PHP-XSL
3. curl must be installed
4. Excel (tested with 2007, should work with 2003 and newer versions as well)
5. or OpenOffice/LibreOffice (tested with 4.x)

# How to use

First, get a Billomat API key:

1. Log into your Billomat account.
2. In the page header, click your user name.
3. Click "API-Schlüssel anzeigen", write it down along with your Billomat ID.

Then open lib/MyBillomatRequest.class.php and enter the API key and the Billomat ID. Save.

Then run

```bash
 php ./main.php
```

The script will fetch your Billomat customers and save them to an XML file.

Please note that you will need to open your spreadsheet program and "manually" open the file as XML files usually
are not associated with spreadsheet programs so you most probably cannot just double-click the file to open it
(in a spreadsheet that is).